package net.chuckc.sensortest.sensor;

public class SensorItem {

    private String name;
    private String type;
    private int id;

    public SensorItem(String name, int id) {
        this.name = name;
        this.id = id;
        this.type = getSensorTypeString(id);
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return name;
    }

    private String getSensorTypeString(int id) {
        switch (id) {
        case -1: return "All";
        case 1:  return "Accelerometer";
        case 2: return "Magnetic Field";
        case 3: return "Orientation";
        case 4: return "Gyroscope";
        case 5: return "Light";
        case 6: return "Pressure";
        case 7: return "Temperature (deprecated)";
        case 8: return "Proximity";
        case 9: return "Gravity";
        case 10: return "Linear Acceleration";
        case 11: return "Rotation";
        case 12: return "Relative Humidity";
        case 13: return "Ambient Temperature";
        case 14: return "Magnetic Field (uncalibrated)";
        case 15: return "Game Rotation";
        case 16: return "Gyroscope (uncalibrated)";
        case 17: return "Significant Motion";
        case 18: return "Step Detector";
        case 19: return "Step Counter";
        case 20: return "Geo-magnetic Rotation";
        case 21: return "Heart Rate";
        default: return "Unknown";
        }
    }
}
